import FormEdit from "./FormEdit.vue";

export default {
  title: "Vuetify/FormEdit",
  component: FormEdit,
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/7.0/vue/configure/story-layout
    layout: "fullscreen",
  },
};

export const Default = {};