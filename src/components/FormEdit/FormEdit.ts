import {defineComponent} from 'vue'
import axios from 'axios'
export default defineComponent({
  name: "FormEdit",
  data() {
    return {
      booleano: true,
      datos: [],
      model: false,
      activado: "",
      urlCorta: "",
      snackbar: {
        show: false,
        message: null,
        color: null,
      },
      identificacion: "",
      overlay: false,
    };
  },
  created() {
    this.getUrls();
  },
  methods: {
    async actualizar() {
      const send = {
        shortUrl: this.urlCorta,
        activa: this.booleano,
      };
      console.log(send);
      await axios
        .put(config.URL + `actualizar/${this.id}`, send)
        .then(data => {
          this.datas = data.data;
          this.snackbar = {
            message: "Se han actualizado los datos de la url",
            color: "green darken-1",
            show: true,
          };
          console.log('okey')
        })
        .catch((err) => {
          this.snackbar = {
            message: "Ops, you have an error. Try again!",
            color: "error",
            show: true,
          };
          console.error(err);
        });
    },
    async getUrls() {
      const res = await axios.get(config.URL + `getId/${this.id}`);
      this.datos = res.data.url2;
      console.log(this.datos.activa);
      this.swichActivar(this.datos.activa);
      this.urlCorta = this.datos.shortUrl;
    },
    swichActivar(act) {
      if (act == true) {
        this.activado = "Url activada";
      }
      if (act == false) {
        this.activado = "Url desactivada";
      }
    },
    cogerId(id) {
      console.log(id);
      this.identificacion = id;
    },
    cambiarActivo(estado) {
      if (estado == true) {
        estado = false;
      } else {
        estado = true;
      }
      this.booleano = estado;
    },
    async eliminar() {
      console.log(this.identificacion);
      await axios.delete(config.URL + `delet/${this.identificacion}`);
    },
  },
});

