import Listado from "./Listado.vue";

export default {
  title: "Vuetify/Listado",
  component: Listado,
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/7.0/vue/configure/story-layout
    layout: "fullscreen",
  },
};

export const Default = {};