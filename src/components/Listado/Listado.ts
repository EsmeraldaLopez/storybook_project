import { defineComponent } from "vue";
import axios from "axios";


export default defineComponent({

  name: 'Listado',

  data() {
    return {     
      urls: [], 
      guardar: [],
      filtrado: [],
      identificacion: [],
      page: 1,
      perPage: 10,
      search: "",
      desAsc: true,
      act: 2,
      overlay: false,
      snackbar: {
        show: false,
        message: null,
        color: null,
      },
    };
  },
  created() {
    this.getUrls();// Coge los urls de la base de datos cuando se accede a la pagina
  },
  mounted() {
    window.addEventListener("resize", this.añadirHeaders);//Detecta cada vez que haya un cambio en el tamaño de la página
  },
  methods: {
    añadirHeaders() {
        //Funcion paraañadir los headers en el responsive cuando no haya datos
      let anchoPantalla = document.documentElement.clientWidth;
      if (anchoPantalla <= 1400 && this.filtrado.length === 0) {
        document.getElementById("informacion").style.display = "block";
      }
    },

    async actualizarActivar(booleano, id) {
        //Swich para actualizar el campo activa
      this.identificacion = id;
      console.log(this.identificacion);
      console.log(booleano);
      const send = {
        activa: booleano,
      };
      await axios
        .put(config.URL + `actuaActiva/${this.identificacion}`, send)
        .then((data) => {
      
          this.snackbar = {
            message: 'Se ha Actualizado',
            color: 'green',
            show: true,
          };
          console.log(data.data);
        })
        .catch((err) => {
          this.snackbar = {
            message: "Ops, you have an error. Try again!",
            color: "error",
            show: true,
          };
          console.error(err);
        });
    },

    async getUrls() {
        //Coger los datos de la base de datos
      const res = await axios.get(config.URL + "datos");
      console.log(res);
      this.guardar = res.data.users;
      this.urls = res.data.users;
    },

    paginar(urls) {
        //Ordenar los registros segun la cantidad que haya por cada pagina
      return urls.slice(
        this.page * this.perPage - this.perPage,
        this.page * this.perPage
      );
    },

    cambio() {
        //Volber a la página 1
      this.page = 1;
    },

    ordenarActivo() {
        //Boton paa seleccionar los activos, los inactivos o todos
      if (this.act == 3) {
        this.urls = this.guardar;
        this.act = 2;
        this.page = 1;
      } else if (this.act == 2) {
        this.urls = this.guardar.filter(({ activa }) =>
          [activa].some((val) => Boolean(val) == false)
        );
        this.page = 1;
        this.act = 1;
      } else if (this.act == 1) {
        this.urls = this.guardar.filter(({ activa }) =>
          [activa].some((val) => Boolean(val) == true)
        );
        this.page = 1;
        this.act = 3;
      }
      console.log(this.urls.length);
    },

    ordenarFechaCrea() {
        //Boton para ordenar fechas de creacion
      if (this.desAsc == true) {
        this.urls = this.urls.sort(
          (a, b) =>
            new Date(b.fechaGabracion).getTime() -
            new Date(a.fechaGabracion).getTime()
        );
        this.desAsc = false;
        this.page = 1;
      } else {
        this.urls = this.urls.sort(
          (a, b) =>
            new Date(a.fechaGabracion).getTime() -
            new Date(b.fechaGabracion).getTime()
        );
        this.desAsc = true;
        this.page = 1;
      }
    },
    ordenarFechaActu() {
        //Boton para ordenar fechas de actualizacion
      if (this.desAsc == true) {
        this.urls = this.urls.sort(
          (a, b) =>
            new Date(b.fechaEdicion).getTime() -
            new Date(a.fechaEdicion).getTime()
        );
        this.desAsc = false;
        this.page = 1;
      } else {
        this.urls = this.urls.sort(
          (a, b) =>
            new Date(a.fechaEdicion).getTime() -
            new Date(b.fechaEdicion).getTime()
        );
        this.desAsc = true;
        this.page = 1;
      }
    },

    cogerId(id) {
        //conseguir la id del registro
      console.log(id);
      this.identificacion = id;
    },

    eliminar() {
      //Eliminar url
      axios
        .delete(config.URL + `delet/${this.identificacion}`)
        .then((data) => {
          this.getUrls();
          this.snackbar = {
            message: 'Se ha Borrado correctamente',
            color: 'green',
            show: true,
          };
          console.log(data.data);
          console.log("hola " + this.page);
          if (this.filtrado.length % 10 == 1) {
            this.page = this.page - 1;
          }
        })
        .catch((err) => {
          this.snackbar = {
            message: "Ops, you have an error. Try again!",
            color: "error",
            show: true,
          };
          console.error(err);
        });
    },
  },
  computed: {
    filteredData() {
      //filtrar datos del buscador
      return this.paginar(
        (this.filtrado = this.urls.filter(({ shortUrl, originalUrl }) =>
          [shortUrl, originalUrl.toLowerCase()].some((val) =>
            val.includes(this.search.toLowerCase())
          )
        )),
        console.log(this.filtrado.length)
      );
    },

    paginas() {
      //Numero de paginas que tiene
      if (this.filtrado.length / this.perPage < 1) {
        this.page = 1;
        return 1;
      }
      if (this.filtrado.length % 10 == 0) {
        return this.filtrado.length / this.perPage;
      }
      return Math.trunc(this.filtrado.length / this.perPage + 1);
    },

    // urlsMostrados() {
    //   return this.paginar(this.urls);
    // },
  },
});
